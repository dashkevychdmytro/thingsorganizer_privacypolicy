## Privacy Policy

Company (“we”, “our” or “us”) respects the privacy of the users of our mobile application (“App”), and is fully committed to protect the personal information that users share with it in connection with the use of our App. 

This SERVICE is provided by company and is intended for use as is.

This page is used to inform application visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.

If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. We will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Application unless otherwise defined in this Privacy Policy.

By accessing or using the Services, you agree to this Privacy Policy, our Terms of Service.

IF YOU DO NOT AGREE TO THIS PRIVACY POLICY, PLEASE DO NOT USE THE SERVICE.

**Information Collection**

When using our Service, we ask certain information from you:

- Personal Information: when you register with us and use the Application, you generally provide your name, email address, age, user name, password and other registration information; transaction-related information, such as when you make purchases, respond to any offers, or download or use applications from us; information you provide us when you contact us for help the email addresses and information you submitted voluntarily; information you enter into our system when using the Application, such as contact information and data in the app.

- Non-Personal Information: It includes but is not limited to your device’s configuration, the package ID and version of the application that you use.

**Information Usage**

The information that we request is will be retained by us and used as described in this privacy policy for the purposes described below:

- to provide our services or information you request;

- to respond to your emails, submissions, questions, comments, requests, and complaints and provide customer service;

- to analyze usage and trends with anonymous user data, and to improve the quality of our service and user experience;

- to send you confirmations, updates, security alerts, and support and administrative messages and otherwise facilitate your use of, and our administration and operation of, our Services;

We may also use the information you provided us to contact your from time to time to provide you with important information, required notices and marketing promotions.

You are welcome to ask for our confirmation whenever you feel concerned about how your personal information is being processed, where and for what purpose.

**Third party services**

The app does use third party services that may collect information used to identify you.

We may work with analytics services to help us understand how the Application is being used, such as the frequency and duration of usage. We work with advertisers and third party advertising networks.

If you’d like to opt-out from third party use of this type of information by the Application, you could do that by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network.

Link to the privacy policy of third party service providers used by the app

-   [Google Play Services - for Android apps](https://www.google.com/policies/privacy/)
-   [Google Analytics](https://policies.google.com/privacy)
-   [AdMob](https://support.google.com/admob/answer/6128543?hl=en)
-   [Firebase](https://firebase.google.com/support/privacy)
-   [Firebase Analytics](https://firebase.google.com/policies/analytics)

**Log Data**

We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.

**Does the Application collect precise real time location information of the device?**

This Application does not collect precise information about the location of your mobile device.

**Cookies**

Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your device internal memory.

This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collection information and to improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.

**Service Providers**

We may employ third-party companies and individuals due to the following reasons:

-   To facilitate our Service;
-   To provide the Service on our behalf;
-   To perform Service-related services; or
-   To assist us in analyzing how our Service is used.

We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.

**Security**

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

**Links to Other Sites**

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

**Children’s Privacy**

We do not use the Application to knowingly solicit data from or market to children under the age of 13. If a parent or guardian becomes aware that his or her child has provided us with information without their consent, he or she should contact us at dashkevich.dmitry.dev@gmail.com We will delete such information from our files within a reasonable time.

**Changes to This Privacy Policy**

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.

**Contact Us**

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us dashkevich.dmitry.dev@gmail.com